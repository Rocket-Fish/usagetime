package com.bearfishapps.usetime;

import android.annotation.TargetApi;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.PixelFormat;
import android.os.Build;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.os.PowerManager;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.TextView;

import java.util.Timer;
import java.util.TimerTask;

public class TimerService extends Service{

    private WindowManager windowManager;
    private TextView chatHead;

    private boolean mHasDoubleClicked = false;
    private long lastPressTime;

    private int x = 0;
    private int y = 0;

    private Handler handler = new Handler(Looper.getMainLooper());

    private PowerManager pm;
    private PowerManager.WakeLock lock;

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    @Override
    public void onCreate() {
        super.onCreate();

        pm = (PowerManager)getSystemService(Context.POWER_SERVICE);
        lock = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, "UseTim_WakeLock");
        lock.acquire();

        //////////////Chathead Core Section
        windowManager = (WindowManager) getSystemService(WINDOW_SERVICE);
        chatHead = new TextView(this);

        int color = Integer.parseInt("000000", 16)+0xFF000000;
        chatHead.setTextColor(color);

        chatHead.setText("-- s");
        chatHead.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
        chatHead.setBackgroundResource(R.drawable.floating2);

        final WindowManager.LayoutParams params = new WindowManager.LayoutParams(
                WindowManager.LayoutParams.WRAP_CONTENT,
                WindowManager.LayoutParams.WRAP_CONTENT,
                WindowManager.LayoutParams.TYPE_PHONE,
                WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE,
                PixelFormat.TRANSLUCENT);

        params.gravity = Gravity.TOP | Gravity.LEFT;
        params.x = 0;
        params.y = 100;

        windowManager.addView(chatHead, params);
        //////////Chathead Core Section

        try {
            chatHead.setOnTouchListener(new View.OnTouchListener() {
                private WindowManager.LayoutParams paramsF = params;
                private int initialX;
                private int initialY;
                private float initialTouchX;
                private float initialTouchY;

                @Override public boolean onTouch(View v, MotionEvent event) {
                    switch (event.getAction()) {
                        case MotionEvent.ACTION_DOWN:

                            // Get current time in nano seconds.
                            long pressTime = System.currentTimeMillis();


                            // If double click...
                            if (pressTime - lastPressTime <= 300) {
                                TimerService.this.stopSelf();

//                                Intent intent = new Intent(TapService.this, MainActivity.class);
//                                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                                startActivity(intent);

                                mHasDoubleClicked = true;
                            }
                            else {     // If not double click....
                                mHasDoubleClicked = false;
                            }
                            lastPressTime = pressTime;
                            initialX = paramsF.x;
                            initialY = paramsF.y;
                            initialTouchX = event.getRawX();
                            initialTouchY = event.getRawY();
                            x = paramsF.x;
                            y = paramsF.y;
                            break;
                        case MotionEvent.ACTION_UP:
                            break;
                        case MotionEvent.ACTION_MOVE:
                            paramsF.x = initialX + (int) (event.getRawX() - initialTouchX);
                            paramsF.y = initialY + (int) (event.getRawY() - initialTouchY);
                            x = paramsF.x;
                            y = paramsF.y;
                            windowManager.updateViewLayout(chatHead, paramsF);
                            break;
                    }
                    return false;
                }
            });
        } catch (Exception e) {
            // TODO: handle exception
        }

        chatHead.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                Log.wtf("On Screen Service", "clicked");

                //				Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                //				intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_SINGLE_TOP|Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                //				getApplicationContext().startActivity(intent);
            }
        });



        SharedPreferences settings = getSharedPreferences("55736554696d655f44617461", 0);
        counter = new CounterClass(pm, settings.getInt("seconds", 0));

        startTimer();

    }

    private CounterClass counter;

    private void startTimer() {
        new Timer().scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                counter.add();
                mHandler.obtainMessage(1).sendToTarget();
            }
        }, 1000, 1000);
    }

    public Handler mHandler = new Handler() {
        public void handleMessage(Message msg) {

            displayTime();
//            chatHead.setText(seconds+"sec"); //this is the textview

            SharedPreferences settings = getSharedPreferences("55736554696d655f44617461", 0);
            SharedPreferences.Editor editor = settings.edit();
            editor.putInt("seconds", counter.getTotalSeconds());
            editor.commit();
        }
    };

    private void displayTime() {
        String display = "";

        if(counter.getDays()>0)
            display+=counter.getDays()+" D ";
        if(counter.getHours()>0)
            display+=counter.getHours()+" h ";
        if(counter.getMinutes()>0 && counter.getDays() == 0)
            display+=counter.getMinutes()+" min ";
        if(counter.getSeconds()>0 && counter.getDays() == 0 && counter.getHours() == 0)
            display+=counter.getSeconds()+" s ";


        chatHead.setText(display);

    }

    @Override
    public void onDestroy() {
        lock.release();
        super.onDestroy();
        if (chatHead != null) windowManager.removeView(chatHead);

    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        // not used
        return null;
    }
}
