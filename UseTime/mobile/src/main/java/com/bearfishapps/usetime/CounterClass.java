package com.bearfishapps.usetime;

import android.os.Build;
import android.os.PowerManager;

public class CounterClass{
    private PowerManager pm;

    private int totalSeconds = 0;

    private int seconds = 0;
    private int minutes = 0;
    private int hours = 0;
    private int days = 0;

    public CounterClass(PowerManager pm, int totalSeconds) {

        this.pm = pm;
        this.totalSeconds = totalSeconds;

        balance();
    }

    private void balance() {
        seconds = totalSeconds%60;
        int temp = totalSeconds/60;
        minutes = temp%60;
        temp = temp/60;
        hours = temp%60;
        temp = temp/60;
        days = temp;


    }


    public void add() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT_WATCH) {
            if(pm.isInteractive()) {
                doAction();
            }
        }
        else {
            if(pm.isScreenOn()) {
                doAction();
            }
        }

    }

    public void doAction() {
        totalSeconds++;

        balance();

        if(seconds%60 == 0 && seconds >0)
            minutes++;
        if(minutes%60 == 0 && minutes >0)
            hours++;
        if(hours%60 == 0 && hours >0)
            days++;

    }

    public int getSeconds() {
        return seconds;
    }

    public int getMinutes() {
        return minutes;
    }

    public int getHours() {
        return hours;
    }

    public int getDays() {
        return days;
    }

    public int getTotalSeconds() {
        return totalSeconds;
    }
}
